import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyModuleRoutingModule } from '@lazy-module/lazy-module-routing.module';
import { LazyModuleRootComponent } from '@lazy-module/pages/components/lazy-module-root/lazy-module-root.component';


@NgModule({
  declarations: [LazyModuleRootComponent],
  imports: [
    CommonModule,
    LazyModuleRoutingModule
  ]
})
export class LazyModuleModule { }
