import { LazyModuleRootComponent } from '@lazy-module/pages/components/lazy-module-root/lazy-module-root.component';

export const COMPONENTS_DECLARATIONS = [LazyModuleRootComponent];

export * from '@lazy-module/pages/components/lazy-module-root/lazy-module-root.component';