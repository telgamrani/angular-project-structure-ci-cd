import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyModuleRootComponent } from './lazy-module-root.component';

describe('LazyModuleRootComponent', () => {
  let component: LazyModuleRootComponent;
  let fixture: ComponentFixture<LazyModuleRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LazyModuleRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyModuleRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
