import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carlo-lazy-module-root',
  templateUrl: './lazy-module-root.component.html',
  styleUrls: ['./lazy-module-root.component.scss']
})
export class LazyModuleRootComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
