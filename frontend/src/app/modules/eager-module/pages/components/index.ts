import { EagerModuleRootComponent } from '@eager-module/pages/components/eager-module-root/eager-module-root.component';

export const COMPONENTS_DECLARATIONS = [EagerModuleRootComponent];

export * from '@eager-module/pages/components/eager-module-root/eager-module-root.component';