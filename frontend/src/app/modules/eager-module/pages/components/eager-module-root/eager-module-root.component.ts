import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carlo-eager-module-root',
  templateUrl: './eager-module-root.component.html',
  styleUrls: ['./eager-module-root.component.scss']
})
export class EagerModuleRootComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {}

}
