import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EagerModuleRootComponent } from './eager-module-root.component';

describe('EagerModuleRootComponent', () => {
  let component: EagerModuleRootComponent;
  let fixture: ComponentFixture<EagerModuleRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EagerModuleRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EagerModuleRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
