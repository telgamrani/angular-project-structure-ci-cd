import { HeaderComponent } from '@core/components/header/header.component';
import { FooterComponent } from '@core/components/footer/footer.component';

export const COMPONENTS_DECLARATIONS = [HeaderComponent, FooterComponent];
export const COMPONENTS_EXPORT = [HeaderComponent, FooterComponent];
