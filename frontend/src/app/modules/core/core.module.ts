import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { COMPONENTS_DECLARATIONS, COMPONENTS_EXPORT } from './components';
import { EagerModuleModule } from '@eager-module/eager-module.module';
import { AppRoutingModule } from '@app-routing/app-routing.module';



@NgModule({
  declarations: [...COMPONENTS_DECLARATIONS], 
  imports: [
    CommonModule,
    EagerModuleModule,
    AppRoutingModule
  ],
  exports: [
    ...COMPONENTS_EXPORT,
    EagerModuleModule,
    AppRoutingModule
  ]
})
export class CoreModule { 

    /* make sure CoreModule is imported only by one NgModule the AppModule */
    constructor (
      @Optional() @SkipSelf() parentModule: CoreModule
    ) {
      if (parentModule) {
        throw new Error('CoreModule is already loaded. Import only in AppModule');
      }
    }

}
